import Title from "../Title/Title";
import { Link, useParams } from 'react-router-dom';
import Donut from "../Donut/Donut";
import styles from './Ready.module.css';

import {AnimatePresence,motion} from "framer-motion/dist/framer-motion";

function Ready() {
    const {id}=useParams();
    console.log(id);
    return (
        <div className={styles.ready}>
            <Donut products={id}/>
            <Title text40='You can now come and pick up your amazing Donut!' />
            <Link to='/'>Done</Link>
        </div>
    );
}

export default Ready;