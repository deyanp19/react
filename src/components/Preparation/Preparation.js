import Title from "../Title/Title";
import React, { useEffect } from 'react';
import {useNavigate, useParams } from "react-router-dom";
import Donut from "../Donut/Donut";
import styles from './Preparation.module.css';

function Preparation() {
    const {id}=useParams()
    const navigate = useNavigate();

    useEffect(() => {
      setTimeout(() => {
        navigate(`/ready/${id}`);
      }, 5000);
    }, [navigate]);
    return (
        <div className={styles.preparation}>
            <Donut products={id}/>
            <Title text48='Preparing &hellip;'></Title>

        </div>
    );
}
export default Preparation;